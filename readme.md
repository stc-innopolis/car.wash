# Car wash service

## Description 

## MVP1

* Main (Гошадзе Александр)
    *  widget quick action
    *  promotion
    *  price

* Map (Викторов Иван)
    *  geo location
    *  mark car wash service
    *  form of request

* Registration Form (Стеценко Александр)
    *  registration
    *  recovery login
    *  restore password

* Price List (Сафин Руслан)
    *  list 
    *  filters
    *  form of request by price element

## install
``` sh
npm install 
```

## run
``` sh
npm start
```
## figma
[Car wash](https://www.figma.com/file/KHsv4aDW1LxB7imdoFZNSp/Car-wash)

## trello
[Car Wash](https://trello.com/b/3VkTlESW/car-wash)

## miro
[Car Wash](https://miro.com/app/board/o9J_lABMwWk=/)

