import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {useTranslation} from 'react-i18next';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';


/**
 * Calculation of the nearest coordinate
 * @param {string} name name.
 * @param {string} price price.
 * @return {name} name
 * @return {price} price
 */
function createData(name, price) {
  return {name, price};
}

const rowsRu = [
  createData('БЕСКОНТАКТНАЯ МОЙКА', 459),
  createData('НАНОМОЙКА', 759),
  createData('МОЙКА ЛЮКС', 1559),
  createData('КОМПЛЕКСНАЯ МОЙКА', 859),
  createData('КОМПЛЕКСНАЯ НАНОМОЙКА', 1159),
  createData('АНТИРЕАГЕНТ', 1400),
  createData('АНТИРЕАГЕНТ КОМПЛЕКС', 1800),
  createData('ЭКСПРЕСС-МОЙКА', 149),
  createData('ПЫЛЕСОС', 250),
  createData('СТЕКЛА', 150),
  createData('ПОЛИРОЛЬ ПЛАСТИКА', 159),
];

const rowsEn = [
  createData('TOUCHLESS CAR WASH', 459),
  createData('NANO WASHING', 759),
  createData('LUXURY WASHING SINKS', 1559),
  createData('COMPLETE WASHING', 859),
  createData('HOLISTIC NANO-WASH', 1159),
  createData('ANTIREAGENT', 1400),
  createData('ANTIREAGENT COMPLEX', 1800),
  createData('EXPRESS WASHING', 149),
  createData('PURPLE', 250),
  createData('GLASS', 150),
  createData('PLASTIC POLISH', 159),
];

export const Prices = () => {
  // eslint-disable-next-line no-unused-vars
  const {t, i18n} = useTranslation();
  const rows = (t('car.wash.lng') === 'ru') ? rowsRu : rowsEn;
  return (
    <>
      <Typography variant="h4" align="center" sx={{fontWeight: 630}} pt={2} pb={2}>
        {t('car.wash.header.price')}
      </Typography>

      <Grid container
        direction="column"
        justifyContent="center"
        alignItems="center"
        pb={5}>
        <TableContainer component={Paper} style={{maxWidth: `50%`}}>
          <Table sx={{minWidth: 630}}>
            <TableHead>
              <TableRow>
                <TableCell style={
                  {fontWeight: 'bold', fontSize: 18}
                }>{t('car.wash.main.service')}</TableCell>
                <TableCell style={
                  {fontWeight: 'bold', fontSize: 18}
                } align='right'>{t('car.wash.main.cost')}, ₽</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                rows.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align='right'>{row.price}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </>
  );
};
