/* eslint-disable camelcase */
import React from 'react';
import {StyledImg, StyledText, Container, CarouselS} from './carousel.styled';
// TODO Компонент карусель переписать на свой
import {Carousel} from '3d-react-carousal';
import {useGetBannersQuery} from '../../__data__/services/api';
import {useTranslation} from 'react-i18next';
import Typography from '@mui/material/Typography';

export const Caro = () => {
  const {t} = useTranslation();
  const {data, isLoading} = useGetBannersQuery(t('car.wash.lng'));
  const slides = data?.banners.map((value)=>(
    <Container key={value}>
      <StyledImg
        /* eslint-disable-next-line max-len */
        src={`${__webpack_public_path__}remote-assets/main/carousel/${value.img}`}
        alt={value.description} />
      <Typography variant="h5">
        <StyledText color={value.color}>{value.title}</StyledText>
      </Typography>

    </Container>));

  return (
    <CarouselS>
      {!isLoading && <Carousel slides={slides} autoplay={true} interval={5000}/> ||
      <Typography variant="h4">
      Loading...
      </Typography>
      }
    </CarouselS>
  );
};
