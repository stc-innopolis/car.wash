/* eslint-disable max-len */
import styled from 'styled-components';

export const StyledImg = styled.img`
        height: 300px;
        width: 400px;
        text-align: center;
`;
export const StyledText = styled.div`
        text-align: 'center';
        font-weight: bold;
        color: ${(props) => props.color ? props.color : 'black'};
        position: absolute;
        top: 120%;
        left: 50%;
        transform: translate(-50%, -50%);
`;
export const Container = styled.div`
  position: relative;
  text-align: center;
`;

export const CarouselS = styled.div`
  height: 435px;
  text-align: center;
  .react-3d-carousel .slider-container .slider-right div{
    border-top: 2px;
    border-right: 2px;
    border-bottom: 2px;
    border-left:  2px;
  };
  
  .react-3d-carousel .slider-container .slider-left div{
    border-top: 2px;
    border-right: 2px;
    border-bottom: 2px;
    border-left:  2px;
  };
`;
