import React from 'react';
import {StyledAchievLine, Div}
  from './achievline.styled';
import {useTranslation} from 'react-i18next';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';

export const AchievLine = () => {
  const {t} = useTranslation();
  return (
    <>
      <Typography variant="h2" align="center" sx={{fontWeight: 650}}>
        {t('car.wash.main.about')}
      </Typography>

      <StyledAchievLine>
        <Div>
          <Paper elevation={2} square>
            <Typography variant="h2" align="center" sx={{fontWeight: 65, padding: 1}}>
              8
            </Typography>
            <Typography variant="body1" align="center" sx={{padding: 1}}>
              {t('car.wash.main.about-1')}
            </Typography>
          </Paper>
        </Div>
        <Div>
          <Paper elevation={6} square>
            <Typography variant="h2" align="center" sx={{fontWeight: 65, padding: 1}}>
              150 000
            </Typography>
            <Typography variant="body1" align="center" sx={{padding: 1}}>
              {t('car.wash.main.about-2')}
            </Typography>
          </Paper>
        </Div>
        <Div>
          <Paper elevation={10} square>
            <Typography variant="h2" align="center" sx={{fontWeight: 65, padding: 1}}>
              1000
            </Typography>
            <Typography variant="body1" align="center" sx={{padding: 1}}>
              {t('car.wash.main.about-3')}
            </Typography>
          </Paper>
        </Div>
        <Div>
          <Paper elevation={10} square>
            <Typography variant="h2" align="center" sx={{fontWeight: 65, padding: 1}}>
              300
            </Typography>
            <Typography variant="body1" align="center" sx={{padding: 1}}>
              {t('car.wash.main.about-4')}
            </Typography>
          </Paper>
        </Div>
        <Div>
          <Paper elevation={6} square>
            <Typography variant="h2" align="center" sx={{fontWeight: 65, padding: 1}}>
              12%
            </Typography>
            <Typography variant="body1" align="center" sx={{padding: 1}}>
              {t('car.wash.main.about-5')}
            </Typography>
          </Paper>
        </Div>
        <Div>
          <Paper elevation={2} square>
            <Typography variant="h2" align="center" sx={{fontWeight: 65, padding: 1}}>
              9000
            </Typography>
            <Typography variant="body1" align="center" sx={{padding: 1}}>
              {t('car.wash.main.about-6')}
            </Typography>
          </Paper>
        </Div>
      </StyledAchievLine>
    </>
  );
};
