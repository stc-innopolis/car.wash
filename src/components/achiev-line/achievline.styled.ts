import styled from 'styled-components';

export const StyledAchievLine = styled.div`
    display: flex;
    justify-content: center;
    margin:2%;
    
`;
export const Div = styled.div`
    display: flex;
    flex-direction:column;
    text-align:center;
    align-items: center;
    margin:1%;
  
`;
export const StyledImg = styled.img`
        height: 150px;
        width: 150px;
        position:relative;
        margin:2%;
       
`;
export const StyledDescription = styled.div`
        font-size: 15px;
        text-align: center;
        font-weight: bold;
        color: ${(props) => props.color ? props.color : 'black'};
        max-width: 150px;
        top:0%;

`;

