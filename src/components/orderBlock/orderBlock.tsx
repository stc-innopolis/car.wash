import React, {useState} from 'react';
import {StyledOrderBlock} from './orderBlock.styled';
import {Form, Field} from 'react-final-form';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import {useTranslation} from 'react-i18next';
import {MenuItem} from '@mui/material';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import {Link} from 'react-router-dom';

export const OrderBlock = () => {
  const [name, setCarWash] = React.useState('');
  const {t} = useTranslation();
  const handleChangeCarWash = (event) => {
    setCarWash(event.target.value);
  };
  let addresses=[];
  if (t('car.wash.main.makeorder') == 'Записаться на мойку') {
    addresses = require('../../../stubs/api/map/map-info-ru.json').address;
  } else {
    addresses = require('../../../stubs/api/map/map-info-en.json').address;
  }
  const [setAlert, useSetAlert] = useState(false);
  function setDefault() {
    useSetAlert(true);
  }
  function SetServices(props) {
    const propsValue = props;
    if (addresses.map((x)=>x.name).find((element) => element == propsValue.value) !== undefined) {
      return (
        <>
          <FormControl fullWidth required>
            <InputLabel id="list-services-label">{t('car.wash.map.choose-service')}</InputLabel>
            <Select
              labelId="list-services-label"
              label={t('car.wash.map.choose-service')}
              sx={{minWidth: '770px'}}
            >
              {addresses[addresses.findIndex((p)=>p.name == propsValue.value)].services.map((x) => (
                <MenuItem title={x} key={x} value={x}>
                  {x}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <br/><br/>
          <FormControl fullWidth required>
            <InputLabel id="available-time-label">{t('car.wash.map.available-time')}</InputLabel>
            <Select
              labelId="available-time-label"
              label={t('car.wash.map.available-time')}
              sx={{minWidth: '770px'}}
            >
              {addresses[addresses.findIndex((p)=>p.name == propsValue.value)].availableTime.map((x) => (
                <MenuItem title={x} key={x} value={x}>
                  {x}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <br /><br />
        </>
      );
    } else {
      return null;
    }
  }

  const onSubmit = async (values) => {
    useSetAlert(true);
    values.phone = '';
  };

  const parsePhone = (value) => {
    const regPlus = /\+/;
    value = value.replace(/(?<!^)\+|\(|\)|\s|-|[a-zA-Zа-яА-Я]/g, '');
    if (regPlus.test(value)) {
      value = value.replace(/\+\d{12,}/g, value.substr(0, 12));
    } else {
      value = value.replace(/\d{11,}/g, value.substr(0, 11));
    }
    return value;
  };

  const validationForm = (values) => {
    const errors = {};
    const regPhone = /^((\+7|7|8|\+8)+([0-9]){10})$/;

    // Валидация номера телефона
    if (!values.phone) {
      // "car.wash.validation.phone.null": "Не заполнено!",
      // "car.wash.validation.phone.incorrect": "Имеет некорректный формат!"
      errors['phone'] = t('car.wash.validation.phone.null');
    } else if (!regPhone.test(values.phone)) {
      errors['phone'] = t('car.wash.validation.phone.incorrect');
    }
    return errors;
  };

  // @ts-ignore
  return (

    <Form
      onSubmit={onSubmit}
      validate={(values) => validationForm(values)}

      render={({handleSubmit, submitting, pristine}) => (
        <div>
          <StyledOrderBlock>
            <Typography variant="h4" align="center" sx={{fontWeight: 650}} pb={8}>
              {t('car.wash.main.makeorder')}
            </Typography>
            <FormControl fullWidth required >
              <InputLabel id="list-names-label">{t('car.wash.map.select')}</InputLabel>
              <Select
                labelId="list-names-label-select"
                label={t('car.wash.map.select')}
                onChange={handleChangeCarWash}
                value={name}
                sx={{minWidth: '770px'}}
              >
                {addresses.map((x) => (
                  <MenuItem title={x.name} key={x.name} value={x.name}>
                    {x.name} &nbsp;<small>{x.address}</small>
                  </MenuItem>
                ))}
              </Select>
            </FormControl >
            <br/><br/>
            <SetServices value={name} />
            <FormControl fullWidth>
              <Stack
                direction="column"
                justifyContent="center"
                alignItems="center"
                spacing={3}
              >
                <Field
                  name="phone"
                  parse={parsePhone}
                  render={({input, meta}) => (
                    <TextField
                      id="telephone"
                      label={t('car.wash.main.number')}
                      variant="outlined"
                      sx={{width: '100%'}}
                      inputProps={{inputMode: 'numeric', pattern: '[0-9]*'}}
                      error={meta.error && meta.touched}
                      helperText={meta.touched ? meta.error : undefined}
                      required
                      {...input}
                    />
                  )}
                />
                <TextField
                  id="outlined-multiline-static"
                  label={t('car.wash.main.comment')}
                  variant="outlined"
                  sx={{width: '100%'}}
                  multiline
                  rows={6}
                />
                {setAlert &&
            <Alert
              variant="outlined"
              severity="success"
            >
              {t('car.wash.victory')}
            </Alert>
                }
              </Stack>
            </FormControl>
            <br /><br />
            {sessionStorage.getItem('login') ?
          <Button
            color='success'
            variant="contained"
            onClick={setDefault}
            disabled={setAlert}
          >{t('car.wash.main.wash')}</Button>:
          <>
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="center"
              spacing={3}
            >
              <Button
                disabled
                color='success'
                variant="contained"
              >{t('car.wash.main.wash')}</Button>
              <Button
                variant="text"
                component={Link}
                to='/car.wash/registration'
              >
                {/* "car.wash.auth.required": "Чтобы записаться на мойку - авторизируйтесь или зарегистрируйтесь!", */}
                {t('car.wash.auth.required')}
              </Button>
            </Stack>
          </>
            }
          </StyledOrderBlock><br /><br />
        </div>
      )}
    />

  );
};
