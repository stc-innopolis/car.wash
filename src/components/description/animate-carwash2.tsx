import React, {useEffect, useRef} from 'react';
import lottie from 'lottie-web';

import animate2 from './CarWash2.json';
import {ContainerAnim2} from './description-styled';

export const AnimationCarWash2 = () => {
  const animContainer = useRef();
  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animContainer.current,
      animationData: animate2,
      autoplay: true,
    });
    return () => anim.destroy();
  }, []);
  return ( <ContainerAnim2>
    <div id='animContainer' ref={animContainer} />
  </ContainerAnim2>);
};

export default AnimationCarWash2;
