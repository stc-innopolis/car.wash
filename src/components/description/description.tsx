import React from 'react';
import {StyledDesc} from './description-styled';
import {useTranslation} from 'react-i18next';
import {Card} from '@mui/material';
import {Button} from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {getNavigations} from '@ijl/cli';
import {AnimationCarWash1} from './animate-carwash1';
import {AnimationCarWash2} from './animate-carwash2';

export const Desc = () => {
  // eslint-disable-next-line no-unused-vars
  const {t, i18n} = useTranslation();
  return (
    <StyledDesc>
      <Card sx={{width: 345}}>
        <AnimationCarWash1 />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {t('car.wash.main.description-1')}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {t('car.wash.main.description-2')}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small"
            onClick={(e) => {
              e.preventDefault();
              window.location.href='https://sam-avtomaster.com/stati-partnerov/vneshnij-vid-avtomobilya';
            }}>{t('car.wash.main.learnmore')}</Button>
        </CardActions>
      </Card>
    &nbsp;&nbsp;&nbsp;
      <Card sx={{maxWidth: 345}}>
        <AnimationCarWash2 />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {t('car.wash.main.description-3')}<a>Car.Wash</a>
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {t('car.wash.main.description-4')}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small"
            onClick={(e) => {
              e.preventDefault();
              window.location.href=getNavigations('car.wash')['link.car.wash.map'];
            }}
          >{t('car.wash.main.onmap')}</Button>

        </CardActions>
      </Card>
    </StyledDesc>
  );
};
