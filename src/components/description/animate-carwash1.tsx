import React, {useEffect, useRef} from 'react';
import lottie from 'lottie-web';

import animate1 from './CarWash1.json';
import {ContainerAnim1} from './description-styled';

export const AnimationCarWash1 = () => {
  const animContainer = useRef();
  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animContainer.current,
      animationData: animate1,
      autoplay: true,
    });
    return () => anim.destroy();
  }, []);
  return ( <ContainerAnim1>
    <div id='animContainer' ref={animContainer} />
  </ContainerAnim1>);
};

export default AnimationCarWash1;
