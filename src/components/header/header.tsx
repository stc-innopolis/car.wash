import React, {useEffect} from 'react';
import {StyledHeader} from './header.styled';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

import {getNavigations} from '@ijl/cli';
import Map from '@mui/icons-material/Map';
import PriceChange from '@mui/icons-material/PriceChange';
import BottomNavigation from '@mui/material/BottomNavigation';
import VpnKey from '@mui/icons-material/VpnKey';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import {Link, useLocation} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Grid from '@mui/material/Grid';

const nav = getNavigations('car.wash');
const URL = {
  base: nav['car.wash'],
  map: nav['link.car.wash.map'],
  main: nav['link.car.wash.main'],
  registration: nav['link.car.wash.registration'],
  price: nav['link.car.wash.price'],
};

export function calcPosition(path) {
  switch (path) {
    case (URL.map):
      return 0;
    case (URL.price):
      return 1;
    case (URL.registration):
      return 2;
    default:
      return 3;
  }
}

export const Header = () => {
  // eslint-disable-next-line no-unused-vars
  const location = useLocation();
  const [value, setValue] = React.useState(calcPosition(location.pathname));
  const {t, i18n} = useTranslation();
  const [alignment, setAlignment] = React.useState(i18n.language);

  useEffect(( ) => {
    setValue(calcPosition(location.pathname));
  }, [location.pathname]);

  const handleChangeLangToEn = async () => {
    await i18n.changeLanguage('en');
    window.location.reload();
  };
  const handleChangeLangToRu = async () => {
    await i18n.changeLanguage('ru');
    window.location.reload();
  };

  const handleChangeLang = (event, newAlignment) => {
    setAlignment(newAlignment);
    if (newAlignment === 'en') {
      handleChangeLangToEn();
    } else {
      handleChangeLangToRu();
    }
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const iconUser = sessionStorage.getItem('login') ?
                <BottomNavigationAction id="PersonOutlineIconIcon" label={sessionStorage.getItem('login')} icon={<PersonOutlineIcon/>} component={Link} to={URL.registration} title={sessionStorage.getItem('login')}/> :
                <BottomNavigationAction id="VpnKeyIcon" label={t('car.wash.header.login-registration')} icon={<VpnKey/>} component={Link} to={URL.registration} title={t('car.wash.header.login-registration')}/>;

  return (<StyledHeader>
    <Box sx={{flexGrow: 1}}>
      <AppBar position="static" color={'inherit'} >
        <Toolbar>

          <Button
            variant="text"
            component={Link}
            to="/car.wash/main"
            size="large"
            color={'inherit'}
            sx={{fontWeight: 'bold', fontSize: 24}}
            title={t('car.wash.header.main-page')}>
            Car wash
          </Button>

          <Typography variant='h4' component={Link} to={URL.base} title={t('car.wash.header.main-page')}
            sx={{fontWeight: 'bold', flexGrow: 1}} color='textPrimary'>
            {/* Car Wash */}
          </Typography>

          <Grid pr={10}>
            <BottomNavigation showLabels value={value} onChange={handleChange}>
              <BottomNavigationAction id="MapIcon" label={t('car.wash.header.map')} icon={<Map/> } component={Link} to={URL.map} title={t('car.wash.header.map')}/>
              <BottomNavigationAction id="PriceChangeIcon" label={t('car.wash.header.price')} icon={<PriceChange />} component={Link} to={URL.price} title={t('car.wash.header.price')}/>
              {iconUser}
            </BottomNavigation>
          </Grid>

          <Grid>
            <ToggleButtonGroup
              color="primary"
              exclusive
              value={alignment}
              onChange={handleChangeLang}>
              <ToggleButton data-testid="ru" value="ru">{t('car.wash.language.ru')}</ToggleButton>
              <ToggleButton data-testid="en" value="en">{t('car.wash.language.en')}</ToggleButton>
            </ToggleButtonGroup>
          </Grid>

        </Toolbar>
      </AppBar>
    </Box>
  </StyledHeader>
  );
};
