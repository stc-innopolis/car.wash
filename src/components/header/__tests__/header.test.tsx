import React from 'react';
import {render,
  screen,
  fireEvent,
} from '@testing-library/react';
import Header from '../';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import {StaticRouter} from 'react-router';
import {calcPosition} from '../header';

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: () => 'text',
        i18n: {},
      };
    },
  };
});

jest.mock('@ijl/cli', () => {
  return {
    getNavigations: () => ({
      'car.wash': '#',
      'link.car.wash.main': '#/main',
      'link.car.wash.map': '#/map',
      'link.car.wash.registration': '#/registration',
      'link.car.wash.price': '#/price',
    }),
    getConfigValue: () => '/api',
  };
});

describe('header', () => {
  it('checkRender', () => {
    const {container} = render(
        <StaticRouter location="car.wash"><Header/></StaticRouter>);
    expect(container).toMatchSnapshot();
    fireEvent.click(screen.getByText('Car wash'));
    expect(container).toMatchSnapshot();
    fireEvent.click(container.querySelector('#MapIcon'));
    expect(container).toMatchSnapshot();
    fireEvent.click(container.querySelector('#VpnKeyIcon'));
    expect(container).toMatchSnapshot();
    fireEvent.click(container.querySelector('#PriceChangeIcon'));
    expect(container).toMatchSnapshot();
    fireEvent.click(screen.queryByTestId('ru'));
    expect(container).toMatchSnapshot();
    fireEvent.click(screen.queryByTestId('en'));
    expect(container).toMatchSnapshot();
  });
  it('calcPosition', () => {
    expect(calcPosition('#')).toBe(3);
    expect(calcPosition('#/map')).toBe(0);
    expect(calcPosition('#/price')).toBe(1);
    expect(calcPosition('#/registration')).toBe(2);
  });
});
