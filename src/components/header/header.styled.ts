import styled from 'styled-components';

export const StyledHeader = styled.div`
    box-shadow: 0px 5px 5px -5px #333;
`;

export const StyledLogoText = styled.h1`
    font: 33px Helvetica;
    margin-left: 10px;
    font-weight: bold;
    padding: 20px;
`;
