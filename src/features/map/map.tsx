import React, {useEffect, useState} from 'react';
import ContentMap from './component/content-map';
import {getConfigValue} from '@ijl/cli';
import {StyledMap} from './map.styled';
import {useTranslation} from 'react-i18next';

export const Map = () => {
  const [ready, setReady] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const {t, i18n} = useTranslation();
  useEffect(() => {
    const element = document.createElement('script');
    element.src= `${getConfigValue('yandex.url.map.key')}=
        ${getConfigValue('yandex.map.key')}&lang=` + t('car.wash.lng.map');
    element.type = 'text/javascript';
    element.async = true;
    element.onload = () => {
      setReady(true);
    };
    document.head.appendChild(element);
    return () => {
      document.head.removeChild(element);
    };
  }, []);

  return (
    <StyledMap>
      {ready && <ContentMap />}
    </StyledMap>
  );
};
