import React from 'react';
import {render} from '@testing-library/react';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import NotFound from '..';

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: () => 'text',
      };
    },
  };
});

jest.mock('../animate', () => {
  return {
    // eslint-disable-next-line react/display-name
    Animation: () => (<div>111</div>),
  };
});

describe('not-found', () => {
  it('checkRender', () => {
    const {container} = render(<NotFound/>);
    expect(container).toMatchSnapshot();
  });
});
