import React from 'react';
import {StyledH2} from './not-found.styled';
import {useTranslation} from 'react-i18next';
import Grid from '@mui/material/Grid';

import {Animation} from './animate';

const NotFound = () => {
  // eslint-disable-next-line no-unused-vars
  const {t} = useTranslation();

  return (
    <div>
      <Grid container
        direction="column"
        justifyContent="center"
        alignItems="center" pt={7}>
        <Grid>
          <Animation />
        </Grid>
        <Grid pt={2}>
          <StyledH2 fontScheme="secondary"
            textAlign="primary"
            fontWeight="secondary">
            {t('car.wash.main.not-found-4')}</StyledH2>
        </Grid>
      </Grid>
    </div>
  );
};

export default NotFound;
