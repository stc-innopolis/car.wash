import React, {useEffect, useRef} from 'react';
import lottie from 'lottie-web';

import animationNotFound from './animation_404.json';
import {ContainerAnim} from './not-found.styled';

export const Animation = () => {
  const animContainer = useRef();
  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animContainer.current,
      animationData: animationNotFound,
      autoplay: true,
    });
    return () => anim.destroy();
  }, []);
  return ( <ContainerAnim>
    <div id='animContainer' ref={animContainer} />
  </ContainerAnim>);
};
