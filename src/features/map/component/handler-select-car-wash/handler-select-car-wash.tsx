/* eslint-disable no-unused-vars */
import React, {useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Slide from '@mui/material/Slide';
import {TransitionProps} from '@mui/material/transitions';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import QRCode from 'qrcode';
import Stack from '@mui/material/Stack';
import {Link} from 'react-router-dom';

interface Props {
  address: Address;
}

interface Address {
  name: string;
  address: string;
  workingMode: string;
  phone: string;
  services: string[];
  availableTime: string[];
};

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
  children: React.ReactElement<any, any>;
},
    ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const HandlerSelectCarWash = ({address}: Props) => {
  const [type, setType] = useState('');
  const [errorType, setErrorType] = useState(false);
  const [time, setTime] = useState('');
  const [errorTime, setErrorTime] = useState(false);
  const [src, setSrc] = useState('');

  useEffect(() => {
    QRCode.toDataURL('CarWash code: ' + Math.floor(Math.random() * 999999)).then(setSrc);
  }, []);


  const handleChangeType = (event) => {
    if (errorType === true) {
      setErrorType(false);
    }
    setType(event.target.value);
  };
  const handleChangeTime = (event) => {
    if (errorTime === true) {
      setErrorTime(false);
    }
    setTime(event.target.value);
  };

  // eslint-disable-next-line no-unused-vars
  const {t, i18n} = useTranslation();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    if (type === '' || time === '') {
      if (type === '') {
        setErrorType(true);
      }
      if (time === '') {
        setErrorTime(true);
      }
    } else {
      setOpen(true);
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <><Grid pt={1}>
      <Typography variant="h5" align="left" sx={{fontWeight: 650}}>
        <>{t('car.wash.map.title')}: {address.name}</>
      </Typography>
      <Typography variant="h6" align="left">
        <>{t('car.wash.map.address')}: {address.address}</>
      </Typography>
      <Typography variant="h6" align="left">
        <>{t('car.wash.map.operating-mode')}: {address.workingMode}</>
      </Typography>
      <Typography variant="h6" align="left" pb={1}>
        <>{t('car.wash.map.phone')}: {address.phone}</>
      </Typography >

      <FormControl fullWidth required >
        <InputLabel id="list-address-label">{t('car.wash.map.choose-service')}</InputLabel>
        <Select
          labelId="list-address-label"
          label={t('car.wash.map.choose-service')}
          error={!!errorType}
          onChange={handleChangeType}
          value={type || ''}>
          {address.services.map((service) => (
            <MenuItem title={service} key={service} value={service}>
              {service}
            </MenuItem>
          ))}
        </Select>
      </FormControl >
      <FormControl margin='normal' fullWidth required>
        <InputLabel id="available-time-label">{t('car.wash.map.available-time')}</InputLabel>
        <Select
          labelId="available-time-label"
          label={t('car.wash.map.available-time')}
          error={!!errorTime}
          onChange={handleChangeTime}
          value={time || ''}>
          {address.availableTime.map((availableTimeWash) => (
            <MenuItem title={availableTimeWash} key={availableTimeWash} value={availableTimeWash}>
              {availableTimeWash}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Grid mt={2}>
        {sessionStorage.getItem('login') ?
        <Button
          variant="contained"
          onClick={handleClickOpen}
          id="button-write"
          disabled
        >
          {t('car.wash.map.write')}
        </Button>:
          <>
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="center"
              spacing={3}
            >
              <Button
                disabled
                color='success'
                variant="contained"
              >{t('car.wash.main.wash')}</Button>
              <Button
                variant="text"
                component={Link}
                to='/car.wash/registration'
              >
                {/* "car.wash.auth.required": "Чтобы записаться на мойку - авторизируйтесь или зарегистрируйтесь!", */}
                {t('car.wash.auth.required')}
              </Button>
            </Stack>
          </>
        }
      </Grid>
    </Grid>

    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="alert-dialog-slide-description"
    >


      <Grid container
        direction='column'
        justifyContent='center'
        alignItems='center'>
        <DialogTitle>{t('car.wash.map.success.title')}</DialogTitle>
        <img src={src} />
      </Grid>
      <DialogContent>
        {/* <DialogContentText id="alert-dialog-slide-description"> */}
        <Typography variant="body1" align="center">
          {t('car.wash.map.success.text')}
        </Typography>

        {/* </DialogContentText> */}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Ok</Button>
      </DialogActions>
    </Dialog>
    </>
  );
};

export default HandlerSelectCarWash;
