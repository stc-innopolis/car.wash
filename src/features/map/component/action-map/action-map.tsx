import React from 'react';
import {StyledActionMap} from './action-map.styled';
import Typography from '@mui/material/Typography';
import HandlerSelectCarWash from '../handler-select-car-wash';
import {useTranslation} from 'react-i18next';


const MessageSelectCarWash = () => {
  // eslint-disable-next-line no-unused-vars
  const {t, i18n} = useTranslation();
  return (
    <Typography ml={10} mt={35} variant="h5" sx={{fontWeight: 650}}>
      {t('car.wash.map.select')}
    </Typography>);
};

interface Props {
  address: any
}

/**
 * Select address with description
 * @return {any} Text description address
 */
function HandlerAction({address}: Props) {
  if (address !== '') {
    return <HandlerSelectCarWash address={address} />;
  } else {
    return <MessageSelectCarWash />;
  }
}

const ActionMap = ({address}: Props) => {
  return (
    <StyledActionMap>
      <HandlerAction address={address} />
    </StyledActionMap>
  );
};
export default ActionMap;
