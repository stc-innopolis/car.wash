import styled from 'styled-components';
import {FONTS} from '../../../../constants/fonts';
import {TEXT_ALIGN} from '../../../../constants/text-align';
import {FONTS_WEIGHT} from '../../../../constants/fonts-weight';

export const ContainerAnim = styled.div`
  height: 550px;
  width: 600px;
  display: block;
  margin-left: auto;
  margin-right: auto;
`;

const h2theme = {
  primary: {
    font: FONTS.primary,
    textAlign: TEXT_ALIGN.primary,
    fontWeight: FONTS_WEIGHT.primary,
  },
  secondary: {
    font: FONTS.fifth,
    textAlign: TEXT_ALIGN.primary,
    fontWeight: FONTS_WEIGHT.secondary,
  },
};


export const StyledH2 = styled.h2`
font: ${(props) => h2theme[props.fontScheme].font};
text-align: ${(props) => h2theme[props.textAlign].textAlign};
font-weight: ${(props) => h2theme[props.fontWeight].fontWeight};
`;
