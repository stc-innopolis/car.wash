import React from 'react';
import {render} from '@testing-library/react';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import ErrorBoundary from '../../error-boundary';
import Header from '../../../../../components/header';

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: () => 'text',
        i18n: {},
      };
    },
  };
});

jest.mock('@ijl/cli', () => {
  return {
    getNavigations: () => '',
  };
});

jest.mock('../animate', () => {
  return {
    // eslint-disable-next-line react/display-name
    Animation: () => (<div>111</div>),
  };
});

describe('error-handler', () => {
  it('checkRender', () => {
    const {container} = render(
        <ErrorBoundary>
          <Header/>
        </ErrorBoundary>);
    expect(container).toMatchSnapshot();
  });
});
