import React, {useEffect, useRef} from 'react';
import lottie from 'lottie-web';

import animationError from './error_500.json';
import {ContainerAnim} from './error-handler.styled';

export const Animation = () => {
  const animContainer = useRef();
  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animContainer.current,
      animationData: animationError,
      autoplay: true,
    });
    return () => anim.destroy();
  }, []);
  return ( <ContainerAnim>
    <div id='animContainer' ref={animContainer} />
  </ContainerAnim>);
};
