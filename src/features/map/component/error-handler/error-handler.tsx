import React from 'react';
import {StyledH2} from './error-handler.styled';
import {Animation} from './animate';


const ErrorHandler = () => {
  return (
    <>
      <Animation/>
      <StyledH2 fontScheme="secondary"
        textAlign="primary"
        fontWeight="secondary">
         Ошибка 500. Похоже что-то пошло не так...
        Мы над этим работаем.</StyledH2>
    </>
  );
};

export default ErrorHandler;
