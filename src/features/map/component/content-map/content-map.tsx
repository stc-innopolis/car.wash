import {StyledContentMap, StyledYandexMap} from './content-map.styled';
import ActionMap from '../action-map';
import React from 'react';
import {yandexMaps} from './yandex-map';
import Grid from '@mui/material/Grid';

const ContentMap = () => {
  return (
    <StyledContentMap>
      <Grid container
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start">
        <Grid item xs={8}>
          <StyledYandexMap id="map"></StyledYandexMap>
        </Grid>
        <Grid container item xs={3} justifyContent="flex-start">
          <ActionMap address={yandexMaps()} />
        </Grid>
      </Grid>
    </StyledContentMap>);
};

export default ContentMap;
