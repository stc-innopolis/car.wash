import {useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {useGetMapByTypeMutation} from '../../../../__data__/services/api';


/**
 * Calculation of the nearest coordinate
 * @param {any} x x coordinate.
 * @param {any} y y coordinate.
 * @param {any} coord2 array from x and y coordinate.
 * @return {any} Calculates the nearest coordinate
 */
function isNearCoordinate(x, y, coord2) {
  const minRange = 0.004;
  const subArr1 = x - coord2[0];
  const subArr2 = y - coord2[1];

  return ((subArr1 < minRange && subArr1 > -minRange) &&
    (subArr2 < minRange && subArr2 > -minRange)) ? true : false;
}

/**
 * Loading yandex maps
 * @param {any} setText Extract text for description.
 * @param {any} address Add adress to .
 */
export function yandexMapsRunner(setText, address) {
  address = address.data.address;
  /**
  * Init yandex maps
  */
  function init() {
    // @ts-ignore
    const myMap = new ymaps.Map('map', {
      center: [55.752933, 48.746247],
      zoom: 13,
    },
    {
      searchControlProvider: 'yandex#search',
    });
    // @ts-ignore
    const carMark = new ymaps.GeoObjectCollection(null, {
      preset: 'islands#greenIcon',
    });
    const carCoords = [];
    address.forEach((el) => {
      carCoords.push([el.x, el.y]);
    });
    for (let i = 0, l = carCoords.length; i < l; i++) {
      // @ts-ignore
      carMark.add(new ymaps.Placemark(carCoords[i]));
    }
    myMap.geoObjects.add(carMark);
    carMark.events.add('click', function(e) {
      address.forEach((el) => {
        if (isNearCoordinate(el.x, el.y, e.get('coords'))) {
          setText(() => {
            return el;
          });
        }
      });
    });
  };
  // @ts-ignore
  ymaps.ready(init);
}

export function yandexMaps() {
  const [text, setText] = useState('');

  // eslint-disable-next-line no-unused-vars
  const {t, i18n} = useTranslation();
  // eslint-disable-next-line no-unused-vars
  const [dataRest, {isLoading}] = useGetMapByTypeMutation(t('car.wash.lng'));
  useEffect(() => {
    (async () => {
      const res = await dataRest(t('car.wash.lng'));
      yandexMapsRunner(setText, res);
    })();
  }, []);
  return text;
}


