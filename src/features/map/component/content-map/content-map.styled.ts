import styled from 'styled-components';

export const StyledContentMap = styled.div`
  padding-left: 10px;
  padding-top: 10px;
`;
export const StyledYandexMap = styled.div`
  height: 90vh;
  width: 90%;
  position: relative;
`;
