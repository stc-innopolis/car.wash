import React, {Component, ErrorInfo, ReactNode} from 'react';
import ErrorHandler from '../error-handler';

interface Props {
  children: ReactNode;
}

interface State {
  hasError: boolean;
}

/**
 * A class "error boundary"
 */
class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false,
  };

  /**
 * Select address with description
 * @param {Error} _ Input error.
 * @return {Boolean} Result "has error"
 */
  public static getDerivedStateFromError(_: Error): State {
    return {hasError: true};
  }

  /**
 * Select address with description
 * @param {Error} error Props with address description.
 * @param {ErrorInfo} errorInfo Props with address description.
 */
  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error('Uncaught error:', error, errorInfo);
  }

  /**
 * Handler error boundary
 * @return {any} Result error boundary
 */
  public render() {
    if (this.state.hasError) {
      return <ErrorHandler/>;
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
