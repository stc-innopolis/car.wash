import React from 'react';
import 'whatwg-fetch';
import {render} from '@testing-library/react';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import {Map} from '../map';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {api} from '../../../__data__/services/api';


jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: () => 'text',
        i18n: {},
      };
    },
  };
});

describe('map', () => {
  it('checkRender', () => {
    const {container} = render(<ApiProvider api={api}><Map/></ApiProvider>);
    expect(container).toMatchSnapshot();
  });
});
