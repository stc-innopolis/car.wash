import React from 'react';
import {render} from '@testing-library/react';
import 'whatwg-fetch';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import {BrowserRouter} from 'react-router-dom';


import HandlerSelectCarWash from '../component/handler-select-car-wash';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {api} from '../../../__data__/services/api';

class AddressI {
  name: string;
  address: string;
  workingMode: string;
  phone: string;
  services: string[];
  availableTime: string[];
}


jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: () => 'text',
        i18n: {},
      };
    },
  };
});

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('address', () => {
  return {
    getConfigValue: () => '/api',
  };
});

describe('handler-select', () => {
  const address: AddressI = new AddressI();

  it('checkRender', () => {
    address.name = 'InnoWashing';
    address.phone = '7654321';
    address.address = 'Россия, Республика Татарстан, Верхнеуслонский район, Иннополис, Спортивная улица, 106';
    address.workingMode = '08: 00–20: 00';
    address.services = ['Бесконтактная мойка'];
    address.availableTime = ['7.00-8.00'];
    const {container} = render(<BrowserRouter><ApiProvider api={api}><HandlerSelectCarWash address={address}/></ApiProvider></BrowserRouter>);
    expect(container).toMatchSnapshot();
  });
});
