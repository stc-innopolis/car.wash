import styled from '@emotion/styled';

export const StyledMain = styled.div`
`;

export const Title = styled.div`
        font-size: 78px;
        font-weight: bold;
        text-align: center;
        padding: 2%;
`;

export const Name = styled.div`
        color: gold;
`;

export const ClearCar = styled.div`
        text-align: center;
`;

