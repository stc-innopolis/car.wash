import React, {useRef} from 'react';
import {StyledMain, Name, ClearCar} from './main.styled';
import Caro from '../../components/carousel';
import AchievLine from '../../components/achiev-line';
import Prices from '../../components/prices';
import Desc from '../../components/description';
import MetaTags from 'react-meta-tags';
import {useTranslation} from 'react-i18next';
import OrderBlock from '../../components/orderBlock';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import {Button} from '@mui/material';


export const Main = () => {
  // eslint-disable-next-line no-unused-vars

  const {t} = useTranslation();
  const myRef = useRef(null);
  const executeScroll = () => myRef.current.scrollIntoView();


  return (
    <>
      <MetaTags>
        <title>Car.Wash</title>
        <meta
          name="description"
          content="Запись на мойку"
        />
        <meta
          property="og:title"
          content="Главная"
        />
      </MetaTags>
      <StyledMain>
        <Grid pt={10}>
          <Typography variant="h2" align="center" sx={{fontWeight: 650}} pb={10}>
            {t('car.wash.main.welcome')}
            <Name> Car.Wash</Name>
          </Typography>
        </Grid>
        <Caro/>
        <ClearCar>
          <Button variant="contained" color="error"
            onClick={executeScroll}> {t('car.wash.main.scrollingbutton')}</Button>
        </ClearCar>
        <Desc/>
        <AchievLine/>
        <Grid container justifyContent="center" id="OrderBlock" ref={myRef}>
          <OrderBlock/>
        </Grid>
        <Prices/>
      </StyledMain>

    </>

  );
};
