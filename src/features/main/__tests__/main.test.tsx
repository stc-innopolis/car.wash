import React from 'react';
import {render, waitFor, screen} from '@testing-library/react';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {BrowserRouter} from 'react-router-dom';

import 'whatwg-fetch';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';

import Main from '../';
import {api} from '../../../__data__/services/api';

const server = setupServer(
    // Describe the requests to mock.
    rest.get('/api/getBanners', (req, res, ctx) => {
      return res(
          ctx.json({
            'status': {
              'code': 0,
            },
            'banners': [
              {
                'img': 'first-slide.jpg',
                'title': 'Быстрое и качественное обслуживание',
                'description': '1',
                'color': 'yellow',
              },
            ],
          }),
      );
    }),
);

jest.mock('../../../components/description/animate-carwash1', () => {
  return {
    // eslint-disable-next-line react/display-name
    AnimationCarWash1: () => (<div>111</div>),
  };
});

jest.mock('../../../components/description/animate-carwash2', () => {
  return {
    // eslint-disable-next-line react/display-name
    AnimationCarWash2: () => (<div>111</div>),
  };
});

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: () => 'text',
        i18n: {},
      };
    },
  };
});

describe('main', () => {
  beforeAll(() => {
    // Establish requests interception layer before all tests.
    server.listen();
  });
  afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    server.close();
  });

  it('checkRender', async () => {
    const {container} = render(<BrowserRouter><ApiProvider api={api}><Main/></ApiProvider></BrowserRouter>);
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Быстрое и качественное обслуживание'));
    expect(container).toMatchSnapshot();
  });
});
