import React, {useState} from 'react';
import RegistrationForm from './components/registration';
import Information from './components/information';
import SignIn from './components/signin';
import PassRecovery from './components/passrecovery';
import LoginRecovery from './components/loginrecovery';
import {useTranslation} from 'react-i18next';
import {Link as LinkReactRouterDom} from 'react-router-dom';

import {
  RegistrationMain,
  RegistrationFormBG,
} from './registration.styled';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';


const Registration = () => {
  const [
    regHeader,
    setRegHeader,
  ] = useState([
    false,
    true,
    false,
    false,
  ]);

  const viewReg = () => {
    setRegHeader([
      true,
      false,
      false,
      false,
    ]);
  };

  const viewLogin = () => {
    setRegHeader([
      false,
      true,
      false,
      false,
    ]);
  };

  const recPass = () => {
    setRegHeader([
      false,
      false,
      true,
      false,
    ]);
  };

  const recLog = () => {
    setRegHeader([
      false,
      false,
      false,
      true,
    ]);
  };

  // eslint-disable-next-line no-unused-vars
  const {t, i18n} = useTranslation();

  return (<>
    {sessionStorage.getItem('login') ?
    <RegistrationMain>
      <Information />
    </RegistrationMain> :
    <RegistrationMain>

      {regHeader[1] &&
        <RegistrationFormBG>
          <SignIn />
          <Grid container direction="column" justifyContent="center">

            <Typography
              align="center"
              variant="body1"
              onClick={viewReg}
              component={LinkReactRouterDom}
              to='#'
            >
              {t('car.wash.auth.registration')}
            </Typography>

            <Typography
              variant="body1"
              onClick={recPass}
              component={LinkReactRouterDom}
              to='#'
              align="center"
            >
              {t('car.wash.auth.recovery.password')}
            </Typography>

            <Typography
              variant="body1"
              onClick={recLog}
              component={LinkReactRouterDom}
              to='#'
              align="center"
            >
              {t('car.wash.auth.recovery.login')}
            </Typography>

          </Grid>
        </RegistrationFormBG>
      }

      {regHeader[0] &&
        <RegistrationFormBG>
          <RegistrationForm />
          <Grid container direction="column" justifyContent="center">
            <Typography
              variant="body1"
              onClick={viewLogin}
              component={LinkReactRouterDom}
              to='#'
              align="center"
            >
              {t('car.wash.auth.authorization')}
            </Typography>
          </Grid>
        </RegistrationFormBG>
      }

      {regHeader[2] && <RegistrationFormBG><PassRecovery /></RegistrationFormBG>}

      {regHeader[3] && <RegistrationFormBG><LoginRecovery /></RegistrationFormBG>}

    </RegistrationMain >
    }
  </>);
};

export default Registration;
