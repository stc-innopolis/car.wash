import React, {useState} from 'react';
import {LoginRecoveryStyled} from './loginrecovery.styled';
import {Link} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import {Form, Field} from 'react-final-form';
import {usePostLoginRecMutation} from '../../../../__data__/services/api';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

export const LoginRecovery = () => {
  // eslint-disable-next-line no-unused-vars

  const {t} = useTranslation();

  const [
    loginRec,
    setloginRec,
  // "car.wash.auth.recorvery.form": "Пожалуйста заполните форму восстановления логина",
  ] = useState(t('car.wash.auth.recorvery.form'));
  // ] = useState(t('car.wash.auth.sign-in') + log);
  // ] = useState(<CircularProgress />);

  // const [
  //   logForm,
  //   setLogForm,
  // // "car.wash.auth.sign-in": "Пожалуйста введите логин и пароль"
  // ] = useState(true);

  const [
    useLoginRec,
  ] = usePostLoginRecMutation();

  const parsePhone = (value) => {
    const regPlus = /\+/;
    value = value.replace(/(?<!^)\+|\(|\)|\s|-|[a-zA-Zа-яА-Я]/g, '');
    if (regPlus.test(value)) {
      value = value.replace(/\+\d{12,}/g, value.substr(0, 12));
    } else {
      value = value.replace(/\d{11,}/g, value.substr(0, 11));
    }
    return value;
  };

  const LoginRecForm =
  <>
    <Grid container direction="row" justifyContent="center" spacing="2">

      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={3}
      >

        <Field
          name="phone"
          parse={parsePhone}
          render={({input, meta}) => (
            <TextField
              sx={{width: '100%'}}
              id="phoneLoginRec"
              required
              error={meta.error && meta.touched}
              helperText={meta.touched ? meta.error : undefined}
              label={t('car.wash.auth.registration.phone')}
              {...input}
            />
          )}
        />

        <Grid container direction="row" justifyContent="center" pt={1}>

          <Grid pl={1} pr={1} pb={1}>
            <Button
              variant="contained"
              type="submit">
              {t('car.wash.auth.recorvery')}
            </Button>
          </Grid>

          <Grid pl={1} pr={1} pb={1}>
            <Button
              variant="contained"
              component={Link}
              to='/car.wash/main'
            >
              {/* "car.wash.auth.cancel": "Отмена" */}
              {t('car.wash.auth.cancel')}
            </Button>
          </Grid>

        </Grid>
      </Stack>
    </Grid>
  </>;

  const SMSForm =
  <>
    <Grid container direction="row" justifyContent="center" spacing="2">

      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={3}
      >

        <Field
          name="phonecode"
          render={({input, meta}) => (
            <TextField
              sx={{width: '100%'}}
              required
              error={meta.error && meta.touched}
              helperText={meta.touched ? meta.error : undefined}
              id="phoneLoginRecCode"
              label={t('car.wash.auth.registration.phone.code')}
              {...input}
            />
          )}
        />

        <Grid container direction="row" justifyContent="center" pt={1}>

          <Grid pl={1} pr={1} pb={1}>
            <Button
              variant="contained"
              type="submit">
              {t('car.wash.auth.recorvery')}
            </Button>
          </Grid>

          <Grid pl={1} pr={1} pb={1}>
            <Button
              variant="contained"
              component={Link}
              to='/car.wash/main'
            >
              {/* "car.wash.auth.cancel": "Отмена" */}
              {t('car.wash.auth.cancel')}
            </Button>
          </Grid>

        </Grid>
      </Stack>
    </Grid>
  </>;

  const LoginFinalForm =
<>
  <Grid container direction="row" justifyContent="center" spacing="2">

    <Stack
      direction="column"
      justifyContent="center"
      alignItems="center"
      spacing={3}
    >

      <Typography variant="h6" align="center" pb={3}>
        {t('car.wash.auth.recovery.login.success')}
      </Typography>

      <Grid container direction="row" justifyContent="center" pt={1}>

        <Grid pl={1} pr={1} pb={1}>
          <Button
            variant='contained'
            component={Link}
            to='/car.wash/main'
          >
            {/* "car.wash.auth.main.page": "На главную" */}
            {t('car.wash.auth.main.page')}
          </Button>
        </Grid>

      </Grid>
    </Stack>
  </Grid>
</>;

  const [
    phoneRecForms,
    setPhoneRecForms,
  ] = useState([LoginRecForm, 0]);

  const onSubmit = async (values) => {
    setloginRec(t('car.wash.auth.loading'));

    if (phoneRecForms[1] === 0) {
      await useLoginRec({
        phone: values.phone,
        phoneRecForm: phoneRecForms[1],
      }).unwrap().then((res) => {
        if (res.status === 'success') {
          setloginRec(res.phonecode);
          setPhoneRecForms([SMSForm, 1]);
        } else {
          // "car.wash.auth.error.phone": "Данный телефон отсутствует"
          setloginRec(t('car.wash.auth.error.phone'));
        }
      });
    }

    if (phoneRecForms[1] === 1) {
      await useLoginRec({
        phone: values.phone,
        phonecode: parseInt(values.phonecode),
        phoneRecForm: phoneRecForms[1],
      }).unwrap().then((res) => {
        if (res.status === 'success') {
          setloginRec(res.login);
          setPhoneRecForms([LoginFinalForm, 1]);
        } else {
          // "car.wash.auth.error.code": "Неверный код подтверждения"
          setloginRec(t('car.wash.auth.error.code'));
        }
      });
    }
  };

  const validationForm = (values) => {
    const errors = {};

    // Валидация кода
    if (phoneRecForms[1] === 1) {
      // "car.wash.validation.code.null": "Не заполнено!",
      // "car.wash.validation.code.incorrect": "Имеет некорректный формат!",
      const regCode = /^[0-9]{4}$/;
      if (!values.phonecode) {
        errors['phonecode'] = t('car.wash.validation.code.null');
      } else if (!regCode.test(values.phonecode)) {
        errors['phonecode'] = t('car.wash.validation.code.incorrect');
      }
    }

    // Валидация телефона
    if (phoneRecForms[1] == 0) {
      // "car.wash.validation.phone.null": "Не заполнено!",
      // "car.wash.validation.phone.incorrect": "Имеет некорректный формат!"
      const regPhone = /^((\+7|7|8|\+8)+([0-9]){10})$/;
      if (!values.phone) {
        errors['phone'] = t('car.wash.validation.phone.null');
      } else if (!regPhone.test(values.phone)) {
        errors['phone'] = t('car.wash.validation.phone.incorrect');
      }
    }

    return errors;
  };

  return (

    <Form
      onSubmit={onSubmit}
      validate={(values) => validationForm(values)}

      render={({handleSubmit, submitting, pristine}) => (
        <LoginRecoveryStyled
          name="loginRecovery"
          onSubmit={handleSubmit}
        >
          <Typography variant="h6" align="center" pb={3}>
            {loginRec}
          </Typography>

          {phoneRecForms[0]}
        </LoginRecoveryStyled>

      )}
    />
  );
};
