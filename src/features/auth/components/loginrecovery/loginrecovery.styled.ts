import styled from 'styled-components';

export const LoginRecoveryStyled = styled.form`
    //TODO: стилизовать форму
`;

export const HeaderForm = styled.div`
    font-family: Verdana, Tahoma, sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    text-align: center;
    color: #273558;
    margin-bottom: 32px;
`;
