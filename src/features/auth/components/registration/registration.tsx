import React, {useState} from 'react';
import {useHistory, Link} from 'react-router-dom';

import {Form, Field} from 'react-final-form';
import {useTranslation} from 'react-i18next';
import {usePostRegistrationMutation} from '../../../../__data__/services/api';

import {RegistrationStyled} from './registration.styled';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';


export const RegistrationForm = () => {
  // eslint-disable-next-line no-unused-vars

  const redirect = useHistory();
  const redirectMain = () => redirect.push('/car.wash/main');

  const {t} = useTranslation();
  const [
    regHeader,
    setRegHeader,
    // "Пожалуйста заполните форму восстановления пароля"
  ] = useState(t('car.wash.auth.registration.fill'));

  const [
    regForm,
    setRegForm,
  ] = useState(true);

  const [
    userRegistration,
  ] = usePostRegistrationMutation();

  const onSubmit = async (values) => {
    setRegHeader(t('car.wash.auth.loading'));

    await userRegistration({
      phone: values.phone,
      login: values.login,
      pass: values.password,
    }).unwrap().then((res) => {
      if (res.status === 'success') {
        //  "car.wash.auth.registration.success": "Регистрация прошла успешно"
        setRegHeader(t('car.wash.auth.registration.success'));
        setRegForm(false);
        setTimeout(redirectMain, 2000);
      } else if (res.status === 'dooble') {
        // "car.wash.auth.registration.dooble": "Пользователь с таким логином уже зарегистрирован!",
        setRegHeader(t('car.wash.auth.registration.dooble'));
      } else if (res.status === 'dooblePhone') {
        // "car.wash.auth.registration.dooble.phone": "Пользователь с таким телефоном уже зарегистрирован!",
        setRegHeader(t('car.wash.auth.registration.dooble.phone'));
      } else {
        // "car.wash.auth.registration.data.error": "Данные некорректны",
        setRegHeader(t('car.wash.auth.registration.data.error'));
      }
    });
  };

  const parsePhone = (value) => {
    const regPlus = /\+/;
    value = value.replace(/(?<!^)\+|\(|\)|\s|-|[a-zA-Zа-яА-Я]/g, '');
    if (regPlus.test(value)) {
      value = value.replace(/\+\d{12,}/g, value.substr(0, 12));
    } else {
      value = value.replace(/\d{11,}/g, value.substr(0, 11));
    }
    return value;
  };

  const validationForm = (values) => {
    const errors = {};
    const regPhone = /^((\+7|7|8|\+8)+([0-9]){10})$/;
    const regLog = /^[a-zA-Z0-9]+$/;
    const regPass = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

    // Валидация номера телефона
    if (!values.phone) {
      // "car.wash.validation.phone.null": "Не заполнено!",
      // "car.wash.validation.phone.incorrect": "Имеет некорректный формат!"
      errors['phone'] = t('car.wash.validation.phone.null');
    } else if (!regPhone.test(values.phone)) {
      errors['phone'] = t('car.wash.validation.phone.incorrect');
    }

    // Валидация логина
    if (!values.login) {
      // "car.wash.validation.login.null": "Не заполнено!",
      // "car.wash.validation.login.minsize": "Не может быть меньше 8 символов!",
      // "car.wash.validation.login.lat": "Должен содержать латинские буквы и цифры!",
      errors['login'] = t('car.wash.validation.login.null');
    } else if (values.login.length < 3) {
      errors['login'] = t('car.wash.validation.login.minsize');
    } else if (!regLog.test(values.login)) {
      errors['login'] = t('car.wash.validation.login.lat');
    }

    // Валидация пароля
    if (!values.password) {
      // "car.wash.validation.pass.null": "Не заполнено!",
      // "car.wash.validation.pass.minsize": "Не может быть меньше 8 символов!",
      // "car.wash.validation.pass.lat": "Должен содержать A-Z, a-z, спецсимвол и цифры!",
      errors['password'] = t('car.wash.validation.pass.null');
    } else if (values.password.length < 8) {
      errors['password'] = t('car.wash.validation.pass.minsize');
    } else if (!regPass.test(values.password)) {
      errors['password'] = t('car.wash.validation.pass.lat');
    }
    return errors;
  };

  return (

    <Form
      onSubmit={onSubmit}
      validate={(values) => validationForm(values)}

      render={({handleSubmit, submitting, pristine}) => (

        <RegistrationStyled
          name="registration"
          onSubmit={handleSubmit}>

          <Typography variant="h6" align="center" pb={6}>
            {regHeader}
          </Typography>

          {regForm &&
            <Grid container direction="row" justifyContent="center" spacing={{xs: 2, md: 2}}>

              <Stack
                direction="column"
                justifyContent="center"
                alignItems="center"
                spacing={3}
              >

                <Field
                  name="phone"
                  parse={parsePhone}
                  render={({input, meta}) => (
                    <TextField
                      sx={{width: '100%'}}
                      error={meta.error && meta.touched}
                      helperText={meta.touched ? meta.error : undefined}
                      id="phone"
                      data-testid="testphone"
                      required
                      label={t('car.wash.auth.registration.phone')}
                      {...input}
                    />
                  )}
                />

                <Field
                  name="login"
                  render={({input, meta}) => (
                    <TextField
                      sx={{width: '100%'}}
                      error={meta.error && meta.touched}
                      helperText={meta.touched ? meta.error : undefined}
                      id="login"
                      data-testid="testlogin"
                      required
                      label={t('car.wash.auth.login')}
                      {...input}
                    />
                  )}
                />

                <Field
                  name="password"
                  render={({input, meta}) => (
                    <TextField
                      sx={{width: '100%'}}
                      error={meta.error && meta.touched}
                      helperText={meta.touched ? meta.error : undefined}
                      id="password"
                      data-testid="testpass"
                      type="password"
                      required
                      label={t('car.wash.auth.enter.password')}
                      {...input}
                    />
                  )}
                />

                <Grid container justifyContent="center" direction="row" rowSpacing={1} columnSpacing={{xs: 1, sm: 2, md: 3}}>
                  <Grid pl={1} pr={1} pb={1}>
                    <Button
                      variant="contained"
                      disabled={submitting || pristine}
                      type="submit"
                    >
                      {/* "car.wash.auth.registration": "Зарегистрироваться" */}
                      {t('car.wash.auth.registration')}
                    </Button>
                  </Grid>
                  <Grid pl={1} pr={1} pb={1}>
                    <Button
                      variant="contained"
                      component={Link}
                      to='/car.wash/main'
                    >
                      {/* "car.wash.auth.cancel": "Отмена" */}
                      {t('car.wash.auth.cancel')}
                    </Button>
                  </Grid>
                </Grid>

              </Stack>

            </Grid>

          }

          {!regForm &&
            <Grid container direction="row" justifyContent="center" spacing={{xs: 2, md: 2}}>

              <Stack
                direction="column"
                justifyContent="center"
                alignItems="center"
                spacing={3}
              >
                <Grid container justifyContent="center" direction="row" rowSpacing={1} columnSpacing={{xs: 1, sm: 2, md: 3}}>
                  <Grid pl={1} pr={1} pb={1}>
                    <Button
                      variant="contained"
                      component={Link}
                      to='/car.wash/main'
                    >
                      {t('car.wash.auth.main.page')}
                    </Button>
                  </Grid>
                </Grid>

              </Stack>

            </Grid>
          }

        </RegistrationStyled>
      )}
    />
  );
};
