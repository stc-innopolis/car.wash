import React, {useState} from 'react';
import {PassRecoveryStyled} from './passrecovery.styled';
import {Link} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import {Form, Field} from 'react-final-form';
import {usePostPassRecMutation} from '../../../../__data__/services/api';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';


export const PassRecovery = () => {
  // eslint-disable-next-line no-unused-vars
  const {t} = useTranslation();

  let code = '';

  const [
    passRec,
    setPassRec,
    // "car.wash.auth.pass.recorvery.fiil": "Пожалуйста заполните форму восстановления пароля",
  ] = useState(t('car.wash.auth.pass.recorvery.fiil'));

  const [
    usePassRec,
  ] = usePostPassRecMutation();

  const PassRecForm =
    <>
      <Grid container direction="row" justifyContent="center" spacing="2">

        <Stack
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={3}
        >

          <Field
            name="login"
            render={({input, meta}) => (
              <TextField
                sx={{width: '100%'}}
                id="loginPassRec"
                required
                error={meta.error && meta.touched}
                helperText={meta.touched ? meta.error : undefined}
                label={t('car.wash.auth.registration.login')}
                {...input}
              />
            )}
          />

          <Grid container direction="row" justifyContent="center" pt={1}>

            <Grid pl={1} pr={1} pb={1}>
              <Button
                variant="contained"
                type="submit">
                {t('car.wash.auth.recorvery')}
              </Button>
            </Grid>

            <Grid pl={1} pr={1} pb={1}>
              <Button
                variant="contained"
                component={Link}
                to='/car.wash/main'
              >
                {/* "car.wash.auth.cancel": "Отмена" */}
                {t('car.wash.auth.cancel')}
              </Button>
            </Grid>

          </Grid>
        </Stack>
      </Grid>
    </>;

  const SMSForm =
    <>
      <Grid container direction="row" justifyContent="center" spacing="2">

        <Stack
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={3}
        >

          <Field
            name="phonecode"
            render={({input, meta}) => (
              <TextField
                sx={{width: '100%'}}
                required
                error={meta.error && meta.touched}
                helperText={meta.touched ? meta.error : undefined}
                id="phonePassRecCode"
                label={t('car.wash.auth.registration.phone.code')}
                {...input}
              />
            )}
          />

          <Grid container direction="row" justifyContent="center" pt={1}>

            <Grid pl={1} pr={1} pb={1}>
              <Button
                variant="contained"
                type="submit">
                {t('car.wash.auth.recorvery')}
              </Button>
            </Grid>

            <Grid pl={1} pr={1} pb={1}>
              <Button
                variant="contained"
                component={Link}
                to='/car.wash/main'
              >
                {/* "car.wash.auth.cancel": "Отмена" */}
                {t('car.wash.auth.cancel')}
              </Button>
            </Grid>

          </Grid>
        </Stack>
      </Grid>
    </>;

  const NewPass =
    <>
      <Grid container direction="row" justifyContent="center" spacing="2">

        <Stack
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={3}
        >

          <Field
            name="phonenewpass"
            render={({input, meta}) => (
              <TextField
                sx={{width: '100%'}}
                required
                error={meta.error && meta.touched}
                helperText={meta.touched ? meta.error : undefined}
                id="phoneNewPass"
                label={t('car.wash.auth.registration.phone.new.pass')}
                type="password"
                {...input}
              />
            )}
          />

          <Grid container direction="row" justifyContent="center" pt={1}>

            <Grid pl={1} pr={1} pb={1}>
              <Button
                variant="contained"
                type="submit">
                {t('car.wash.auth.recorvery')}
              </Button>
            </Grid>

            <Grid pl={1} pr={1} pb={1}>
              <Button
                variant="contained"
                component={Link}
                to='/car.wash/main'
              >
                {/* "car.wash.auth.cancel": "Отмена" */}
                {t('car.wash.auth.cancel')}
              </Button>
            </Grid>

          </Grid>
        </Stack>
      </Grid>
    </>;

  const PassFinalForm =
    <>
      <Grid container direction="row" justifyContent="center" spacing="2">

        <Stack
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={3}
        >

          <Grid container direction="row" justifyContent="center" pt={1}>

            <Grid pl={1} pr={1} pb={1}>
              <Button
                variant='contained'
                component={Link}
                to='/car.wash/main'
              >
                {/* "car.wash.auth.main.page": "На главную" */}
                {t('car.wash.auth.main.page')}
              </Button>
            </Grid>

          </Grid>
        </Stack>
      </Grid>
    </>;

  const [
    phoneRecForms,
    setPhoneRecForms,
  ] = useState([PassRecForm, 0]);

  const onSubmit = async (values) => {
    setPassRec(t('car.wash.auth.loading'));
    if (phoneRecForms[1] === 0) {
      await usePassRec({
        login: values.login,
        phoneRecForm: phoneRecForms[1],
      }).unwrap().then((res) => {
        if (res.status === 'success') {
          setPassRec(res.phonecode);
          code = res.phonecode;
          setPhoneRecForms([SMSForm, 1]);
        } else {
          // "car.wash.auth.error.login": "Данный логин не зарегистрирован",
          setPassRec(t('car.wash.auth.error.login'));
        }
      });
    }

    if (phoneRecForms[1] === 1) {
      await usePassRec({
        login: values.login,
        phonecode: parseInt(values.phonecode),
        phoneRecForm: phoneRecForms[1],
      }).unwrap().then((res) => {
        if (res.status === 'success') {
          setPassRec(t('car.wash.auth.registration.phone.new.pass'));
          setPhoneRecForms([NewPass, 2]);
        } else {
          // "car.wash.auth.error.phone": "Данный телефон отсутствует",
          setPassRec(t('car.wash.auth.error.phone'));
        }
      });
    }

    if (phoneRecForms[1] === 2) {
      await usePassRec({
        login: values.login,
        phonecode: parseInt(code),
        phonenewpass: values.phonenewpass,
        phoneRecForm: phoneRecForms[1],
      }).unwrap().then((res) => {
        if (res.status === 'success') {
          setPassRec(t('car.wash.auth.registration.phone.new.pass.success'));
          setPhoneRecForms([PassFinalForm, 3]);
        } else {
          // "car.wash.auth.error.code": "Неверный код подтверждения",
          setPassRec(t('car.wash.auth.error.code'));
        }
      });
    }
  };

  const validationForm = (values) => {
    const errors = {};

    // Валидация кода
    if (phoneRecForms[1] === 1) {
      // "car.wash.validation.code.null": "Не заполнено!",
      // "car.wash.validation.code.incorrect": "Имеет некорректный формат!",
      const regCode = /^[0-9]{4}$/;
      if (!values.phonecode) {
        errors['phonecode'] = t('car.wash.validation.code.null');
      } else if (!regCode.test(values.phonecode)) {
        errors['phonecode'] = t('car.wash.validation.code.incorrect');
      }
    }

    // Валидация логина
    if (phoneRecForms[1] === 0) {
      // "car.wash.validation.login.null": "Не заполнено!",
      // "car.wash.validation.login.minsize": "Не может быть меньше 8 символов!",
      // "car.wash.validation.login.lat": "Должен содержать латинские буквы и цифры!",
      const regLog = /^[a-zA-Z0-9]+$/;
      if (!values.login) {
        errors['login'] = t('car.wash.validation.login.null');
      } else if (values.login.length < 3) {
        errors['login'] = t('car.wash.validation.login.minsize');
      } else if (!regLog.test(values.login)) {
        errors['login'] = t('car.wash.validation.login.lat');
      }
    }

    // Валидация пароля
    if (phoneRecForms[1] === 2) {
      // "car.wash.validation.pass.null": "Не заполнено!",
      // "car.wash.validation.pass.minsize": "Не может быть меньше 8 символов!",
      // "car.wash.validation.pass.lat": "Должен содержать A-Z, a-z, спецсимвол и цифры!",
      const regPass = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
      if (!values.phonenewpass) {
        errors['phonenewpass'] = t('car.wash.validation.pass.null');
      } else if (values.phonenewpass.length < 8) {
        errors['phonenewpass'] = t('car.wash.validation.pass.minsize');
      } else if (!regPass.test(values.phonenewpass)) {
        errors['phonenewpass'] = t('car.wash.validation.pass.lat');
      }
    }
    return errors;
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={(values) => validationForm(values)}


      render={({handleSubmit, submitting, pristine}) => (
        <PassRecoveryStyled
          name="passRecovery"
          onSubmit={handleSubmit}
        >
          <Typography variant="h6" align="center" pb={3}>
            {passRec}
          </Typography>

          {phoneRecForms[0]}
        </PassRecoveryStyled>

      )}
    />
  );
};

