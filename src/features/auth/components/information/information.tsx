import React from 'react';
import {useTranslation} from 'react-i18next';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import {useHistory} from 'react-router-dom';

export const Information = () => {
  // eslint-disable-next-line no-unused-vars

  const redirect = useHistory();
  const redirectMain = () => redirect.push('/car.wash/main');

  const {t} = useTranslation();

  return (
    <>
      <Typography variant="h6" align="center" pb={3}>
        {/* "car.wash.auth.login": "Логин", */}
        {t('car.wash.auth.login')} : {sessionStorage.getItem('login')}
      </Typography>

      <Typography variant="h6" align="center" pb={3}>
        {/* "car.wash.auth.login": "Логин", */}
        <Button
          variant="contained" onClick={() => {
            sessionStorage.removeItem('login');
            setTimeout(redirectMain, 1000);
          }}>
          {t('car.wash.auth.recorvery.exit')}
        </Button>
      </Typography>
    </>
  );
};
