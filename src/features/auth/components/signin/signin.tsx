import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
// import {NavLink} from 'react-router-dom';

import {Form, Field} from 'react-final-form';
import {usePostAuthorizationMutation} from '../../../../__data__/services/api';
import {useTranslation} from 'react-i18next';

import {SignInStyled} from './signin.styled';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import {Link} from 'react-router-dom';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
// import CircularProgress from '@mui/material/CircularProgress';


export const SignIn = () => {
  // eslint-disable-next-line no-unused-vars

  const redirect = useHistory();
  const redirectMain = () => redirect.push('/car.wash/main');

  const {t} = useTranslation();

  const [
    signIn,
    setsignIn,
    // "car.wash.auth.sign-in": "Пожалуйста введите логин и пароль"
  ] = useState(t('car.wash.auth.sign-in'));
  // ] = useState(<CircularProgress />);

  const [
    logForm,
    setLogForm,
    // "car.wash.auth.sign-in": "Пожалуйста введите логин и пароль"
  ] = useState(true);

  const [
    userAuthorization,
  ] = usePostAuthorizationMutation();

  const onSubmit = async (values) => {
    setsignIn(t('car.wash.auth.loading'));
    // setsignIn(<CircularProgress />);

    await userAuthorization({
      login: values.login,
      pass: values.password,
    }).unwrap().then((res) => {
      if (res.status === 'success') {
        // "car.wash.auth.enter": "Вход выполнен"
        // setsignIn(t('car.wash.auth.enter'));
        setsignIn(res.login);
        setLogForm(false);
        sessionStorage.setItem('login', res.login);
        setTimeout(redirectMain, 2000);
      } else {
        // "car.wash.auth.error.enter": "Неверный логин или пароль"
        setsignIn(t('car.wash.auth.error.enter'));
      }
    });
  };

  const validationForm = (values) => {
    const errors = {};

    // Валидация логина
    if (!values.login) {
      // "car.wash.validation.login.null": "Не заполнено!",
      errors['login'] = t('car.wash.validation.login.null');
    }

    // Валидация пароля
    if (!values.password) {
      // "car.wash.validation.pass.null": "Не заполнено!",
      errors['password'] = t('car.wash.validation.pass.null');
    }
    return errors;
  };

  return (

    <Form
      onSubmit={onSubmit}
      validate={(values) => validationForm(values)}

      render={({handleSubmit, submitting, pristine}) => (

        <SignInStyled
          name="signIn"
          onSubmit={handleSubmit}
        >
          <Typography variant="h6" align="center" pb={6}>
            {signIn}
          </Typography>

          {logForm &&
            <Grid container direction="row" justifyContent="center" spacing={{xs: 2, md: 2}}>

              <Stack
                direction="column"
                justifyContent="center"
                alignItems="center"
                spacing={3}
              >

                <Field
                  name="login"
                  render={({input, meta}) => (
                    <TextField
                      sx={{width: '100%'}}
                      error={meta.error && meta.touched}
                      helperText={meta.touched ? meta.error : undefined}
                      required
                      id="loginSignIn"
                      label={t('car.wash.auth.login')}
                      {...input}
                    />
                  )}
                />

                <Field
                  name="password"
                  render={({input, meta}) => (
                    <TextField
                      sx={{width: '100%'}}
                      error={meta.error && meta.touched}
                      helperText={meta.touched ? meta.error : undefined}
                      required
                      type="password"
                      id="passwordSignIn"
                      label={t('car.wash.auth.enter.password')}
                      {...input}
                    />
                  )}
                />

                <Grid container justifyContent="center" direction="row" rowSpacing={1} columnSpacing={{xs: 1, sm: 2, md: 3}}>
                  <Grid pl={1} pr={1} pb={1}>
                    <Button
                      variant="contained"
                      disabled={submitting || pristine}
                      type="submit">
                      {/* "car.wash.auth.authorization": "Авторизироваться" */}
                      {t('car.wash.auth.authorization')}
                    </Button>
                  </Grid>
                  <Grid pl={1} pr={1} pb={1}>
                    <Button
                      variant="contained"
                      component={Link}
                      to='/car.wash/main'
                    >
                      {/* "car.wash.auth.cancel": "Отмена" */}
                      {t('car.wash.auth.cancel')}
                    </Button>
                  </Grid>
                </Grid>

              </Stack>

            </Grid>

          }

          {!logForm &&
          <Grid container direction="row" justifyContent="center" spacing={{xs: 2, md: 2}}>

            <Stack
              direction="column"
              justifyContent="center"
              alignItems="center"
              spacing={3}
            >
              <Grid container justifyContent="center" direction="row" rowSpacing={1} columnSpacing={{xs: 1, sm: 2, md: 3}}>
                <Grid pl={1} pr={1} pb={1}>
                  <Button
                    variant='contained'
                    component={Link}
                    to='/car.wash/main'
                  >
                    {/* "car.wash.auth.main.page": "На главную" */}
                    {t('car.wash.auth.main.page')}
                  </Button>
                </Grid>
              </Grid>

            </Stack>

          </Grid>
          }

        </SignInStyled>
      )}
    />
  );
};
