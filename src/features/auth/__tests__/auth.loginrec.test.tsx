import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


const server = setupServer(
    rest.post('/api/auth/postLoginRec', (req, res, ctx) => {
      return res(
          ctx.json({
            status: 'success',
            phonecode: '1111',
            login: 'qwertyu',
          }),
      );
    }),
);
// const server2 = setupServer(
//     rest.post('/api/auth/postLoginRec', (req, res, ctx) => {
//       return res(
//           ctx.json({
//             status: 'success',
//             login: 'qwertyu',
//           }),
//       );
//     }),
// );

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const recLogin = [
  {
    phoneRecLogin: [
      '89121094284',
    ],
    phoneRecCode: [
      '1111',
    ],
  },
  {
    phoneRecLogin: [
      '+79121094284',
    ],
    phoneRecCode: [
      '1111',
    ],
  },
];

describe('testLoginRecClick', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });

  it.each(recLogin)('LoginRecClick', async (recLogin) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли логин?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления логина'));
    expect(container).toMatchSnapshot();
    const inputPhone = container.querySelector('#phoneLoginRec');
    fireEvent.change(inputPhone, {
      target: {
        value: recLogin.phoneRecLogin,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getByText('1111'));
    expect(container).toMatchSnapshot();
    const inputCode = container.querySelector('#phoneLoginRecCode');
    fireEvent.change(inputCode, {
      target: {
        value: recLogin.phoneRecCode,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getByText('qwertyu'));
    expect(container).toMatchSnapshot();
  });
});

// describe('testInformation', () => {
//   it('Information', async () => {
//     const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

//     await fireEvent.click(screen.getByText('Забыли пароль?'));
//     expect(container).toMatchSnapshot();
//     await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления пароля'));
//     expect(container).toMatchSnapshot();
//     await fireEvent.click(screen.getByText('Восстановить'));
//     expect(container).toMatchSnapshot();
//     await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления пароля'));
//     expect(container).toMatchSnapshot();
//   });
// });

