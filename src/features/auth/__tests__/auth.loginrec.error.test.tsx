import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


const server = setupServer(
    rest.post('/api/auth/postLoginRec', (req, res, ctx) => {
      return res(
          ctx.json({
            status: 'error',
          }),
      );
    }),
);

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const recLogin = [
  {
    phoneRecLogin: [
      '89121094284',
    ],
    plusPhoneRecLogin: [
      '+79121094284',
    ],
    badPhoneRecLogin: [
      '5656565656',
    ],
    phoneRecCode: [
      '1111',
    ],
    badPhoneRecCode: [
      '2222',
    ],
  },
];

describe('testLoginRecClickErrPhone', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });

  it.each(recLogin)('LoginRecClickErrPhone', async (recLogin) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли логин?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления логина'));
    expect(container).toMatchSnapshot();
    const inputPhone = container.querySelector('#phoneLoginRec');
    fireEvent.change(inputPhone, {
      target: {
        value: recLogin.phoneRecLogin,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Данный телефон отсутствует'));
    expect(container).toMatchSnapshot();
  });

  it.each(recLogin)('LoginRecClickErrPhonePlus', async (recLogin) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли логин?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления логина'));
    expect(container).toMatchSnapshot();
    const inputPhonePlus = container.querySelector('#phoneLoginRec');
    fireEvent.change(inputPhonePlus, {
      target: {
        value: recLogin.plusPhoneRecLogin,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Данный телефон отсутствует'));
    expect(container).toMatchSnapshot();
  });

  it.each(recLogin)('LoginRecClickErrFormat', async (recLogin) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли логин?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления логина'));
    expect(container).toMatchSnapshot();
    const inputPhone2 = container.querySelector('#phoneLoginRec');
    fireEvent.change(inputPhone2, {
      target: {
        value: recLogin.badPhoneRecLogin,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Имеет некорректный формат!'));
    expect(container).toMatchSnapshot();
  });
});
