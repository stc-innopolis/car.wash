import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const loginArray = [
  {
    inDataLogin: [
      'qwertyu',
    ],
    outDataLogin: [
      'qwertyu',
    ],
  },
  {
    inDataLogin: [
      'qw',
    ],
    outDataLogin: [
      'qw',
    ],
  },
  {
    inDataLogin: [
      'йцйцйц',
    ],
    outDataLogin: [
      'йцйцйц',
    ],
  },
];

const passArray = [
  {
    inDataPass: [
      'Qwerty-11',
    ],
    outDataPass: [
      'Qwerty-11',
    ],
  },
  {
    inDataPass: [
      'Qwerty',
    ],
    outDataPass: [
      'Qwerty',
    ],
  },
];

describe('registration', () => {
  it.each(loginArray)('testLogin', async (login) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    const inputLogin = container.querySelector('#loginSignIn');
    fireEvent.change(inputLogin, {
      target: {
        value: login.inDataLogin,
      },
    });
    await waitFor(() => screen.getByDisplayValue(login.outDataLogin));
    expect(container).toMatchSnapshot();
  });

  it.each(passArray)('testPass', async (pass) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    const inputPass = container.querySelector('#passwordSignIn');
    fireEvent.change(inputPass, {
      target: {
        value: pass.inDataPass,
      },
    });
    await waitFor(() => screen.getByDisplayValue(pass.outDataPass));
    expect(container).toMatchSnapshot();
  });
});
