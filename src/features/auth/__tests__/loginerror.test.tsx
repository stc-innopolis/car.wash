import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const server2 = setupServer(
    // Describe the requests to mock.
    rest.post('/api/auth/postAuthorization', (req, res, ctx) => {
      return res(
          ctx.json({
            status: 'error',
          }),
      );
    }),
);

const signInArray = [
  {
    inDataLogin: [
      'qwertyu',
    ],
    outDataLogin: [
      'qwertyu',
    ],
    inDataPass: [
      'Qwerty-11',
    ],
    outDataPass: [
      'Qwerty-11',
    ],
  },
];

describe('loginerror', () => {
  beforeAll(() => {
    // Establish requests interception layer before all tests.
    server2.listen();
  });
  afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    server2.close();
  });

  it.each(signInArray)('loginerror', async (signin) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    const inputLogin2 = container.querySelector('#loginSignIn');
    fireEvent.change(inputLogin2, {
      target: {
        value: signin.inDataLogin,
      },
    });
    await waitFor(() => screen.getByDisplayValue(signin.outDataLogin));
    expect(container).toMatchSnapshot();

    const inputPass2 = container.querySelector('#passwordSignIn');
    fireEvent.change(inputPass2, {
      target: {
        value: signin.inDataPass,
      },
    });
    await waitFor(() => screen.getByDisplayValue(signin.outDataPass));
    expect(container).toMatchSnapshot();

    await fireEvent.click(screen.getByText('Авторизироваться'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Неверный логин или пароль'));
    expect(container).toMatchSnapshot();
  });
});
