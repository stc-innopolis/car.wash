import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


const server = setupServer(
    // Describe the requests to mock.
    rest.post('/api/auth/postAuthorization', (req, res, ctx) => {
      return res(
          ctx.json({
            status: 'success',
            login: 'qwertyu',
          }),
      );
    }),
);

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const signInArray = [
  {
    inDataLogin: [
      'qwertyu',
    ],
    outDataLogin: [
      'qwertyu',
    ],
    inDataPass: [
      'Qwerty-11',
    ],
    outDataPass: [
      'Qwerty-11',
    ],
  },
];

describe('registration', () => {
  beforeAll(() => {
    // Establish requests interception layer before all tests.
    server.listen();
  });
  afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    server.close();
  });

  it.each(signInArray)('testSignIn', async (signin) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    const inputLogin = container.querySelector('#loginSignIn');
    fireEvent.change(inputLogin, {
      target: {
        value: signin.inDataLogin,
      },
    });
    await waitFor(() => screen.getByDisplayValue(signin.outDataLogin));
    expect(container).toMatchSnapshot();

    const inputPass = container.querySelector('#passwordSignIn');
    fireEvent.change(inputPass, {
      target: {
        value: signin.inDataPass,
      },
    });
    await waitFor(() => screen.getByDisplayValue(signin.outDataPass));
    expect(container).toMatchSnapshot();

    await fireEvent.click(screen.getByText('Авторизироваться'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('qwertyu'));
    expect(container).toMatchSnapshot();
  });
});
