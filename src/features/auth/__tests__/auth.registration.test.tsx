import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

describe('testRegistrationClick', () => {
  it('RegistrationClick', async () => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните регистрационную форму'));
    expect(container).toMatchSnapshot();

    await fireEvent.click(screen.getByText('Авторизироваться'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста введите логин и пароль'));
    expect(container).toMatchSnapshot();
  });
});
