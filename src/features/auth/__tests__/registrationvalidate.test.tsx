import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {
  describe,
  it,
  expect,
  jest,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const phoneArray = [
  {
    inDataPhone: [
      '+79121021111',
    ],
    outDataPhone: [
      '+79121021111',
    ],
  },
  {
    inDataPhone: [
      '89121021111',
    ],
    outDataPhone: [
      '89121021111',
    ],
  },
  {
    inDataPhone: [
      '89121021111999999999',
    ],
    outDataPhone: [
      '89121021111',
    ],
  },
  {
    inDataPhone: [
      '8912102',
    ],
    outDataPhone: [
      '8912102',
    ],
  },
];

const loginArray = [
  {
    inDataLogin: [
      'qwertyu',
    ],
    outDataLogin: [
      'qwertyu',
    ],
  },
  {
    inDataLogin: [
      'qw',
    ],
    outDataLogin: [
      'qw',
    ],
  },
  {
    inDataLogin: [
      'йцйцйц',
    ],
    outDataLogin: [
      'йцйцйц',
    ],
  },
];

const passArray = [
  {
    inDataPass: [
      'Qwerty-11',
    ],
    outDataPass: [
      'Qwerty-11',
    ],
  },
  {
    inDataPass: [
      'Qwerty',
    ],
    outDataPass: [
      'Qwerty',
    ],
  },
  {
    inDataPass: [
      'Qwqwqwqwqw',
    ],
    outDataPass: [
      'Qwqwqwqwqw',
    ],
  },
];

describe('registration', () => {
  it('checkRender', () => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);
    expect(container).toMatchSnapshot();
  });

  it.each(phoneArray)('testPhone', async (phone) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();

    const inputPhone = container.querySelector('#phone');
    fireEvent.change(inputPhone, {
      target: {
        value: phone.inDataPhone,
      },
    });
    await waitFor(() => screen.getByDisplayValue(phone.outDataPhone) || screen.getByText('не заполено!') || screen.getByText(' имеет некорректный формат!'));
    expect(container).toMatchSnapshot();

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();
  });

  it.each(loginArray)('testLogin', async (login) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();

    const inputLogin = container.querySelector('#login');
    fireEvent.change(inputLogin, {
      target: {
        value: login.inDataLogin,
      },
    });
    await waitFor(() => screen.getByDisplayValue(login.outDataLogin) || screen.getByText('не заполено!') || screen.getByText(' не может быть меньше 3 символов!') || screen.getByText(' должен содержать латинский буквы и цифры!'));
    expect(container).toMatchSnapshot();

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();
  });

  it.each(passArray)('testPass', async (pass) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();

    const inputPass = container.querySelector('#password');
    fireEvent.change(inputPass, {
      target: {
        value: pass.inDataPass,
      },
    });
    await waitFor(() => screen.getByDisplayValue(pass.outDataPass) || screen.getByText('не заполено!') || screen.getByText(' не может быть меньше 8 символов!') || screen.getByText(' должен содержать латинские буквы в верхнем и нижнем регистре, спец-символ и цифры!'));
    expect(container).toMatchSnapshot();

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();
  });
});
