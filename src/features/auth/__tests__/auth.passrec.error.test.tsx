import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


const server = setupServer(
    // Describe the requests to mock.
    rest.post('/api/auth/postPassRec', (req, res, ctx) => {
      return res(
          ctx.json({
            status: 'error',
            phonecode: '1111',
            login: 'qwertyu',
          }),
      );
    }),
);

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const recPass = [
  {
    loginRecPass: [
      'qwertyu2',
    ],
    loginRecCode: [
      '1111',
    ],
    newRecPass: [
      'Qwerty-11',
    ],
  },
];

const recPass2 = [
  {
    loginRecPass: [
      'qq',
    ],
    loginRecCode: [
      '',
    ],
    newRecPass: [
      '',
    ],
  },
];

const recPass3 = [
  {
    loginRecPass: [
      'йййййй',
    ],
    loginRecCode: [
      '',
    ],
    newRecPass: [
      '',
    ],
  },
];

describe('testPassRecClick', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });

  it.each(recPass)('PassRecClick', async (recPass) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли пароль?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления пароля'));
    expect(container).toMatchSnapshot();
    const inputLogin = container.querySelector('#loginPassRec');
    fireEvent.change(inputLogin, {
      target: {
        value: recPass.loginRecPass,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getByText('Данный логин не зарегистрирован'));
    expect(container).toMatchSnapshot();
  });

  it.each(recPass2)('PassRecClick2', async (recPass2) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли пароль?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления пароля'));
    expect(container).toMatchSnapshot();
    const inputLogin = container.querySelector('#loginPassRec');
    fireEvent.change(inputLogin, {
      target: {
        value: recPass2.loginRecPass,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getByText('Не может быть меньше 3 символов!'));
    expect(container).toMatchSnapshot();
  });

  it.each(recPass3)('PassRecClick2', async (recPass3) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли пароль?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления пароля'));
    expect(container).toMatchSnapshot();
    const inputLogin = container.querySelector('#loginPassRec');
    fireEvent.change(inputLogin, {
      target: {
        value: recPass3.loginRecPass,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getByText('Должен содержать латинские буквы и цифры!'));
    expect(container).toMatchSnapshot();
  });
});

