import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


const server = setupServer(
    // Describe the requests to mock.
    rest.post('/api/auth/postRegistration', (req, res, ctx) => {
      return res(
          ctx.json({
            login: 'qwertyu',
            pass: 'Qwerty-11',
            phone: '+79121021111',
            status: 'success',
          }),
      );
    }),
);

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const registrationArray = [
  {
    inDataLogin: [
      'qwertyu',
    ],
    inDataPass: [
      'Qwerty-11',
    ],
    inDataPhone: [
      '+79121021111',
    ],
  },
];

describe('registration', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });

  it.each(registrationArray)('testRegistration', async (registration) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();

    const inputPhone = container.querySelector('#phone');
    fireEvent.change(inputPhone, {
      target: {
        value: registration.inDataPhone,
      },
    });

    const inputLogin = container.querySelector('#login');
    fireEvent.change(inputLogin, {
      target: {
        value: registration.inDataLogin,
      },
    });

    const inputPass = container.querySelector('#password');
    fireEvent.change(inputPass, {
      target: {
        value: registration.inDataPass,
      },
    });

    fireEvent.click(screen.getByText('Зарегистрироваться'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('На главную'));
    expect(container).toMatchSnapshot();
  });
});
