import React from 'react';
import {render, fireEvent, screen, waitFor} from '@testing-library/react';
import 'whatwg-fetch';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import {
  describe,
  it,
  expect,
  jest,
  beforeAll,
  afterAll,
} from '@jest/globals';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {BrowserRouter} from 'react-router-dom';

import Registration from '../registration';
import {api} from '../../../__data__/services/api';
import locales from '../../../../locales/ru.json';


const server = setupServer(
    // Describe the requests to mock.
    rest.post('/api/auth/postPassRec', (req, res, ctx) => {
      return res(
          ctx.json({
            status: 'success',
            phonecode: '1111',
            login: 'qwertyu',
          }),
      );
    }),
);

jest.mock('@ijl/cli', () => {
  return {
    getConfigValue: () => '/api',
  };
});

jest.mock('react-i18next', () => {
  return {
    useTranslation: () => {
      return {
        t: (key) => locales[key],
        i18n: {},
      };
    },
  };
});

const recPass = [
  {
    loginRecPass: [
      'qwertyu',
    ],
    loginRecCode: [
      '1111',
    ],
    loginRecCode1: [
      'fdfdf',
    ],
    newRecPass: [
      'Qwerty-11',
    ],
    newRecPass2: [
      'Qwe',
    ],
    newRecPass3: [
      'укукуку',
    ],
  },
];

describe('testPassRecClick', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });

  it.each(recPass)('PassRecClick', async (recPass) => {
    const {container} = render(<ApiProvider api={api}><BrowserRouter><Registration/></BrowserRouter></ApiProvider>);

    await fireEvent.click(screen.getByText('Забыли пароль?'));
    expect(container).toMatchSnapshot();
    await waitFor(() => screen.getByText('Пожалуйста заполните форму восстановления пароля'));
    expect(container).toMatchSnapshot();
    const inputLogin = container.querySelector('#loginPassRec');
    fireEvent.change(inputLogin, {
      target: {
        value: recPass.loginRecPass,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getByText('1111'));
    expect(container).toMatchSnapshot();
    const inputCode = container.querySelector('#phonePassRecCode');
    fireEvent.change(inputCode, {
      target: {
        value: recPass.loginRecCode1,
      },
    });
    const inputCode1 = container.querySelector('#phonePassRecCode');
    fireEvent.change(inputCode1, {
      target: {
        value: recPass.loginRecCode,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getAllByText('Введите новый пароль'));
    expect(container).toMatchSnapshot();
    const inputNewPass2 = container.querySelector('#phoneNewPass');
    fireEvent.change(inputNewPass2, {
      target: {
        value: recPass.newRecPass2,
      },
    });

    await waitFor(() => screen.getAllByText('Введите новый пароль'));
    expect(container).toMatchSnapshot();
    const inputNewPass3 = container.querySelector('#phoneNewPass');
    fireEvent.change(inputNewPass3, {
      target: {
        value: recPass.newRecPass3,
      },
    });

    await waitFor(() => screen.getAllByText('Введите новый пароль'));
    expect(container).toMatchSnapshot();
    const inputNewPass = container.querySelector('#phoneNewPass');
    fireEvent.change(inputNewPass, {
      target: {
        value: recPass.newRecPass,
      },
    });
    await fireEvent.click(screen.getByText('Восстановить'));
    expect(container).toMatchSnapshot();

    await waitFor(() => screen.getByText('Пароль успешно заменен'));
    expect(container).toMatchSnapshot();
  });
});

