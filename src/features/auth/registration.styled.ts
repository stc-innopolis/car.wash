import styled from 'styled-components';

export const RegistrationMain = styled.div`
    width: 100vw;
    padding: 30px 0px;
    justify-content: center;
    align-items: center;
`;

export const RegistrationFormBG = styled.div`
    width: 450px;
    margin: 0px auto 30px;
    background-color: white;
    border-radius: 5px;
    padding: 28px 50px;
`;
