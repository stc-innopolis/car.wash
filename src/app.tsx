import React from 'react';
import Registration from './features/auth';
import Map from './features/map';
import Main from './features/main';
import Header from './components/header';
import {getNavigations} from '@ijl/cli';
import {BrowserRouter, Redirect, Switch, Route} from 'react-router-dom';
import Prices from './components/prices';
import ErrorBoundary from './features/map/component/error-boundary';
import NotFound from './features/map/component/not-found';
import {ApiProvider} from '@reduxjs/toolkit/dist/query/react';
import {api} from './__data__/services/api';


const nav = getNavigations('car.wash');
const URL = {
  base: nav['car.wash'],
  map: nav['link.car.wash.map'],
  main: nav['link.car.wash.main'],
  registration: nav['link.car.wash.registration'],
  price: nav['link.car.wash.price'],
};

const App = () => {
  return (
    <ApiProvider api={api}>
      <BrowserRouter >
        <ErrorBoundary>
          <Header />
        </ErrorBoundary>
        <ErrorBoundary>
          <Switch>
            <Route path={URL.map} component={Map} />
            <Route path={URL.main} component={Main} />
            <Route path={URL.registration} component={Registration} />
            <Route path={URL.price} component={Prices} />
            <Route exact path={URL.base}>
              <Redirect to={URL.main} />
            </Route>
            <Route component={NotFound} />
          </Switch>
        </ErrorBoundary>
      </BrowserRouter>
    </ApiProvider>
  );
};

export default App;

