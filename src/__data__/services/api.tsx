import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {getConfigValue} from '@ijl/cli';

export const api = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: `${getConfigValue('car.wash.api')}`,
  }),
  endpoints: (builder) => ({
    getMapByType: builder.mutation({
      query: (language) => `/get-map-info?lng=${language}`,
    }),
    getBanners: builder.query({
      query: (language) => `/getBanners?lng=${language}`,
    }),
    postRegistration: builder.mutation({
      query: (body) => ({
        url: 'auth/postRegistration',
        method: 'POST',
        body,
      }),
    }),
    postAuthorization: builder.mutation({
      query: (body) => ({
        url: '/auth/postAuthorization',
        method: 'POST',
        body,
      }),
    }),
    postLoginRec: builder.mutation({
      query: (body) => ({
        url: '/auth/postLoginRec',
        method: 'POST',
        body,
      }),
    }),
    postPassRec: builder.mutation({
      query: (body) => ({
        url: '/auth/postPassRec',
        method: 'POST',
        body,
      }),
    }),
  }),

});

export const {useGetMapByTypeMutation, useGetBannersQuery, usePostRegistrationMutation, usePostAuthorizationMutation, usePostLoginRecMutation, usePostPassRecMutation} = api;
