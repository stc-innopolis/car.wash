/* eslint-disable no-unused-vars */
export enum FONTS {
  primary = '30px Helvetica',
  secondary = '14px Helvetica',
  third = '10px Helvetica',
  fourth = '8px Helvetica',
  fifth = '24px Helvetica'
}
