/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  coverageDirectory: '<rootDir>/reports/coverage',
  collectCoverageFrom: [
    '**/src/**/*.{ts,tsx}',
    '!**/src/i18next/**',
    '!**/src/constants/**',
    '!**/src/app.tsx',
    '!**/src/index.tsx',
    '!**/src/features/map/component/not-found/animate.tsx',
    '!**/src/features/map/component/error-handler/animate.tsx',
    '!**/src/features/map/component/content-map/yandex-map.tsx',
    '!**/src/components/description/animate-carwash1.tsx',
    '!**/src/components/description/animate-carwash2.tsx',
    '!**/src/features/map/component/action-map/action-map.tsx',
  ],
  coverageReporters: [
    ['html', {
      subdir: 'html',
    }],
  ],
  moduleNameMapper: {
    '\\.(png|jpg)$': '<rootDir>/__mocks__/file',
  },
  globals: {
    __webpack_public_path__: '',
  },
};
