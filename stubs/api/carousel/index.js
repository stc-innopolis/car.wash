// eslint-disable-next-line new-cap
const router = require('express').Router();

router.get('/getBanners', (req, res) => {
  switch (req.query.lng) {
    case 'en':
      res.send(require('./carousel-en.json'));
      break;
    case 'ru':
      res.send(require('./carousel-ru.json'));
      break;
    case 'en-En':
      res.send(require('./carousel-en.json'));
      break;
    case 'ru-Ru':
      res.send(require('./carousel-ru.json'));
      break;
    default:
      console.log('Unknown language');
  }
});

module.exports = router;
