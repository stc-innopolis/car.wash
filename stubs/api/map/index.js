// eslint-disable-next-line new-cap
const router = require('express').Router();

router.get('/get-map-info', (req, res) => {
  switch (req.query.lng) {
    case 'en':
      res.send(require('./map-info-en.json'));
      break;
    case 'ru':
      res.send(require('./map-info-ru.json'));
      break;
    case 'en-En':
      res.send(require('./map-info-en.json'));
      break;
    case 'ru-Ru':
      res.send(require('./map-info-ru.json'));
      break;
    default:
      console.log('Unknown language');
  }
});

module.exports = router;
