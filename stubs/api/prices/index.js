// eslint-disable-next-line new-cap
const router = require('express').Router();

router.get('/get-prices', (req, res) => {
  switch (req.query.lng) {
    case 'en':
      res.send(require('./prices-en.json'));
      break;
    case 'ru':
      res.send(require('./prices-ru.json'));
      break;
    case 'en-En':
      res.send(require('./prices-en.json'));
      break;
    case 'ru-Ru':
      res.send(require('./prices-en.json'));
      break;
    default:
      console.log('Unknown language');
  }
});
module.exports = router;
