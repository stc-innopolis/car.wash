// eslint-disable-next-line new-cap
const router = require('express').Router();

router.use(require('./login'));
router.use(require('./registration'));
router.use(require('./logrec'));
router.use(require('./passrec'));

module.exports = router;
