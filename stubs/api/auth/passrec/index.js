// eslint-disable-next-line new-cap
const router = require('express').Router();

router.post('/postPassRec', (req, res) => {
  if (req.body.login in req.session) {
    if (req.body.phoneRecForm === 0) {
      const code = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;

      const ret1 = {
        status: 'success',
        phonecode: code,
      };
      req.session[req.body.login].phonecode = ret1.phonecode;
      return res.send(ret1);
    }
    if (req.body.phoneRecForm === 1) {
      if (req.session[req.body.login].phonecode === req.body.phonecode) {
        const ret = {
          login: req.session[req.body.login].login,
          phone: req.session[req.body.login].phone,
          phonecode: req.session[req.body.login].phonecode,
          status: 'success',
        };
        return res.send(ret);
      } else {
        const ret = {
          phonecode: req.session[req.body.login].phonecode,
          status: 'err',
        };
        return res.send(ret);
      }
    }
    if (req.body.phoneRecForm === 2) {
      const ret = {
        status: 'success',
      };
      req.session[req.body.login].phonecode = undefined;
      req.session[req.body.login].pass = req.body.phonenewpass;
      return res.send(ret);
    }
  }
  res.send(require('./error.json'));
});

module.exports = router;
