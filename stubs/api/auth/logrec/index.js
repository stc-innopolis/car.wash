// eslint-disable-next-line new-cap
const router = require('express').Router();

router.post('/postLoginRec', (req, res) => {
  if (Object.getOwnPropertyNames(req.session).some((item) => item === req.body.phone)) {
    const loginRec = req.session[req.body.phone].login;
    if (req.body.phoneRecForm === 0) {
      if (req.session[loginRec].phone === req.body.phone) {
        const code = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
        const ret1 = {
          status: 'success',
          phonecode: code,
        };
        req.session[loginRec].phonecode = ret1.phonecode;
        return res.send(ret1);
      } else {
        res.send(require('./error.json'));
      }
    }

    if (req.body.phoneRecForm === 1) {
      if (req.session[loginRec].phonecode === req.body.phonecode) {
        const ret = {
          login: req.session[loginRec].login,
          phone: req.session[loginRec].phone,
          status: 'success',
        };
        req.session[loginRec].phonecode = undefined;
        return res.send(ret);
      } else {
        const ret = {
          phonecode: req.session[loginRec].phonecode,
          status: 'error',
        };
        return res.send(ret);
      }
    }
  }
  res.send(require('./error.json'));
});

module.exports = router;
