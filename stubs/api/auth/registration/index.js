// eslint-disable-next-line new-cap
const router = require('express').Router();

router.post('/postRegistration', (req, res) => {
  if (req.body.phone && req.body.login && req.body.pass) {
    if (!req.session[req.body.login] && !req.session[req.body.phone]) {
      const ret = {
        login: req.body.login,
        pass: req.body.pass,
        phone: req.body.phone,
        status: 'success',
      };
      req.session[req.body.login] = ret;
      req.session[req.body.phone] = ret;
    } else if (req.session[req.body.login]) {
      return res.send(require('./dooble.json'));
    } else if (req.session[req.body.phone]) {
      return res.send(require('./dooblephone.json'));
    }
    return res.send(require('./success.json'));
  }
  res.send(require('./error.json'));
});

module.exports = router;
