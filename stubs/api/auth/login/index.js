// eslint-disable-next-line new-cap
const router = require('express').Router();

router.post('/postAuthorization', (req, res) => {
  if (req?.session?.[req.body.login]?.['login']) {
    const login = req.session[req.body.login]['login'];
    const pass = req.session[req.body.login]['pass'];
    if (login === req.body.login && pass === req.body.pass) {
      return res.send(req.session[req.body.login]);
    }
  }

  res.send(require('./error.json'));
});

module.exports = router;
